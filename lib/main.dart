import 'package:componentes/src/pages/home_temp.dart';
import 'package:flutter/material.dart';
import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/pages/avatar_page.dart';
import 'package:componentes/src/pages/card_page.dart';
import 'package:componentes/src/pages/animated_page.dart';
import 'package:componentes/src/pages/input_page.dart';
import 'package:componentes/src/pages/slider_page.dart';
import 'package:componentes/src/pages/list_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: "Material App",
      debugShowCheckedModeBanner: false,
      //home: HomePageTemp(),
      initialRoute: '/',
      routes: {
        '/': (context) => HomePageTemp(),
        'alert': (context) => AlertPage(),
        'avatar': (context) => AvatarPage(),
        'card': (context) => CardAndTarjetPage(),
        'animatedContainer': (context) => AnimatedContainerPage(),
        'inputs': (context) => InputsPage(),
        'sliderAndChecks': (context) => SliderAndCheckPage(),
        'listAndScroll': (context) => ListAndScrollPage(),
      },
    );
  }
}
