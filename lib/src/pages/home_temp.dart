import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';
//TextStyle styleText = TextStyle(color: Colors.red, fontSize: 25.0);

class HomePageTemp extends StatelessWidget {
  var options = [
    {"title": 'Alertas', 'icon': Icons.add_alert},
    {"title": 'Avatars', 'icon': Icons.accessibility},
    {"title": 'Cards - Tarjetas', 'icon': Icons.folder_open},
    {"title": 'Animated Container', 'icon': Icons.donut_large},
    {"title": 'Inputs', 'icon': Icons.input},
    {"title": 'Slider - Checks', 'icon': Icons.tune},
    {"title": 'Lista y Scroll', 'icon': Icons.list},
  ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
      ),
      body: _lista(),
    );
  }

  Widget _lista() {
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        //print('builder');
        //print(snapshot.data);
        return ListView(children: _listaItems(snapshot.data, context));
      },
    );
  }

  List<Widget> _listaItems(List<dynamic> data, context) {
    final List<Widget> opciones = [];

    data.forEach((opt) {
      final widgetTemp = ListTile(
        title: Text(opt['text']),
        leading: getIcon(opt['icon']),
        trailing: Icon(Icons.arrow_forward_ios),
        onTap: () {
          print(opt['ruta']);
          Navigator.pushNamed(context, opt['ruta']);
          Fluttertoast.showToast(
              msg: "Presionaste ${opt['text']}",
              toastLength: Toast.LENGTH_SHORT,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.greenAccent,
              textColor: Colors.white,
              fontSize: 16.0);
        },
      );

      opciones..add(widgetTemp)..add(Divider());
    });
    return opciones;
  }

  /* List<Widget> _crearItemsCortal() {
    return options.map((e) {
      return Column(
        children: <Widget>[
          ListTile(
              title: Text(e['title'].toString()),
              leading: Icon(e['icon'], color: Colors.blue),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: Colors.blue,
              ),
              onTap: () => {
                    Fluttertoast.showToast(
                        msg: "Presionaste ${e['title']}",
                        toastLength: Toast.LENGTH_SHORT,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.greenAccent,
                        textColor: Colors.white,
                        fontSize: 16.0)
                  }),
          Divider()
        ],
      );
    }).toList();
  } */
}
