import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Bienvenido a Avatars",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
            ),
            RaisedButton(
              onPressed: () {},
              child: Container(
                child: Text('Boton de Avatars'),
              ),
            ),
            CircleAvatar(
              backgroundColor: Colors.blue,
              child: Text('CL'),
            ),
            CircleAvatar(
              backgroundColor: Colors.greenAccent,
              child: Text('GE'),
            ),
            CircleAvatar(
              backgroundColor: Colors.yellowAccent,
              child: Text('LU'),
            ),
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
              child: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop(true);
              })
        ],
      ),
    );
  }
}
