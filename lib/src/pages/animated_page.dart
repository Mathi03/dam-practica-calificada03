import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AnimatedContainerState();
  }
}

class _AnimatedContainerState extends State<AnimatedContainerPage> {
  double height = 40.0;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Animated Container'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Bienvenido a Animated Container",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
            ),
            RaisedButton(
              child: Container(
                child: Text('Cambiar valor Container'),
              ),
              onPressed: () {
                setState(() {
                  height = 320.0;
                });
              },
            ),
            AnimatedContainer(
              duration: Duration(seconds: 5),
              width: 60.0,
              height: height,
              color: Color(0xff14ff65),
            ),
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
              child: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop(true);
              }),
        ],
      ),
    );
  }
}
