import 'package:flutter/material.dart';

class ListAndScrollPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var options = [
      {"title": 'Alertas', 'icon': Icons.add_alert},
      {"title": 'Avatars', 'icon': Icons.accessibility},
      {"title": 'Cards - Tarjetas', 'icon': Icons.folder_open},
      {"title": 'Animated Container', 'icon': Icons.donut_large},
      {"title": 'Inputs', 'icon': Icons.input},
      {"title": 'Slider - Checks', 'icon': Icons.tune},
      {"title": 'Lista y Scroll', 'icon': Icons.list},
    ];
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('List and Scroll'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Bienvenido a List and Scroll",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
            ),
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
              child: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop(true);
              })
        ],
      ),
    );
    List<Widget> _crearItemsCortal() {
      return options.map((e) {
        return Column(
          children: <Widget>[
            ListTile(
                title: Text(e['title'].toString()),
                leading: Icon(e['icon'], color: Colors.blue),
                trailing: Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.blue,
                ),
                onTap: () => {}),
            Divider()
          ],
        );
      }).toList();
    }
  }
}
